<?php
/**
 * Файл login.php для неавторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 *
 */

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login']))
{
    // Если есть логин в сессии, то пользователь уже авторизован.
    // Делаем перенаправление на форму.
    header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    ?>

    <form action="" method="post">
        <input name="login" />
        <input name="pass" />
        <input type="submit" value="Войти" />
    </form>

    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else
{

    // TODO: Проверть есть ли такой логин и пароль в базе данных.
    // Выдать сообщение об ошибках.
    $user = 'u17494';
    $pass = '2172210';
    $db = 'u17494';
    $host = 'localhost';

    $queryL = "SELECT * FROM anketaWithPass where login='" . (string) $_POST['login'] . "'";

    $link = mysqli_connect($host, $user, $pass, $db) or die("Ошибка " . mysqli_error($link));

    $result = mysqli_query($link, $queryL);

    $row = mysqli_fetch_row($result);

    if ($row[10] != ((string)md5((string)$_POST['pass']))) {
        print ("<div>Неверный пароль! Верный: ". $row[10] . ". Попробуйте ещё раз</div>");
        exit();
    }

    mysqli_close($link);

    // Если все ок, то авторизуем пользователя.
    $_SESSION['login'] = $_POST['login'];

    // Делаем перенаправление.
    header('Location: ./');
}