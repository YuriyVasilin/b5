<?php
// Указываем кодировку для браузера
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{

    $messages = array(); // Массив для временного хранения сообщений пользователю.
    if (!empty($_COOKIE['save']))
    { // Если есть параметр save, то выводим сообщение пользователю.
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';

        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass']))
        {
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.', strip_tags($_COOKIE['login']) , strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['date'] = !empty($_COOKIE['date_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    $errors['super'] = !empty($_COOKIE['super_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);

    // Выдаем сообщения об ошибках.
    if ($errors['name'])
    {
        setcookie('name_error', '', 100000);
        $messages[] = '<div class="error">Укажите имя.</div>';
    }

    if ($errors['email'])
    {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Адрес эл.почты указан неверно. Образец: exp@mail.ru</div>';
    }

    if ($errors['date'])
    {
        setcookie('date_error', '', 100000);
        $messages[] = '<div class="error">Дата рождения указана неверно. Образец: ДД.ММ.ГГГГ</div>';
    }

    if ($errors['gender'])
    {
        setcookie('gender_error', '', 100000);
        $messages[] = '<div class="error">Укажите пол.</div>';
    }

    if ($errors['limb'])
    {
        setcookie('limb_error', '', 100000);
        $messages[] = '<div class="error">Укажите число конечностей.</div>';
    }

    if ($errors['super'])
    {
        setcookie('super_error', '', 100000);
        $messages[] = '<div class="error">Укажите суперспособности.</div>';
    }

    if ($errors['check'])
    {
        setcookie('check_error', '', 100000);
        $messages[] = '<div class="error">Пожалуйста, ознакомьтесь с контрактом!</div>';
    }

    $er = !($errors['name'] || $errors['email'] || $errors['date'] || $errors['gender']
        || $errors['limb'] || $errors['super'] || $errors['check']);

    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    $values = array();
    if ($er && !empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login']))
    {
        // TODO: загрузить данные пользователя из БД
        $user = 'u17494';
        $pass = '2172210';
        $db = 'u17494';
        $host = 'localhost';

        $link = mysqli_connect($host, $user, $pass, $db) or die("Ошибка " . mysqli_error($link));

        $query = "SELECT * FROM anketaWithPass where login='" . $_SESSION['login'] . "'";

        $result = mysqli_query($link, $query);
        if ($result)
        {
            $row = mysqli_fetch_row($result);

            // и заполняем переменную $values

            $values['name'] = $row[1];
            $values['email'] = $row[2];
            $values['date'] = $row[3];
            $values['gender'] = $row[4];
            $values['limb'] = $row[5];
            $values['super'] = $row[6];
            $values['message'] = $row[7];
            $values['check'] = $row[8];
        }
        else
        {
            $messages[] = '<div class="error">Не удалось подключиться к базе!</div>';
            exit();
        }

        // очищаем результат
        mysqli_free_result($result);

        mysqli_close($link);

        printf('Вход с логином %s', $_SESSION['login']);
    }

    else
    {
        $values = array();
        $values['name'] = empty($_COOKIE['name']) ? '' : $_COOKIE['name'];
        $values['email'] = empty($_COOKIE['email']) ? '' : $_COOKIE['email'];
        $values['date'] = empty($_COOKIE['date']) ? '' : $_COOKIE['date'];
        $values['gender'] = empty($_COOKIE['gender']) ? '' : $_COOKIE['gender'];
        $values['limb'] = empty($_COOKIE['limb']) ? '' : $_COOKIE['limb'];
        $values['super'] = empty($_COOKIE['super']) ? '' : $_COOKIE['super'];
        $values['message'] = empty($_COOKIE['message']) ? '' : $_COOKIE['message'];
        $values['check'] = empty($_COOKIE['check']) ? '' : $_COOKIE['check'];
    }

    // Включаем содержимое файла site.php
    include ('site.php');
}

// Иначе, если запрос был методом POST
else
{
    // Проверяем ошибки.
    $errors = false;
    if (empty($_POST['name']))
    {
        // Выдаем куку на день с флажком об ошибке в поле name.
        setcookie('name_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    }

    if (!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $_POST['email']))
    {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    }

    if (!preg_match('/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/', $_POST['date']))
    {
        setcookie('date_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    }

    if (empty($_POST['gender']))
    {
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    }

    if (empty($_POST['limb']))
    {
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    }

    if (!isset($_POST['super1']) && !isset($_POST['super2']) && !isset($_POST['super3']) && !isset($_POST['super4']))
    {
        setcookie('super_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    }

    if (empty($_POST['check']))
    {
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = true;
    }

    if ($errors)
    {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        setcookie('name', $_POST['name'], time() + 365 * 30 * 24 * 60 * 60);
        setcookie('email', $_POST['email'], time() + 365 * 30 * 24 * 60 * 60);
        setcookie('date', $_POST['date'], time() + 365 * 30 * 24 * 60 * 60);
        setcookie('gender', $_POST['gender'], time() + 365 * 30 * 24 * 60 * 60);
        setcookie('limb', $_POST['limb'], time() + 365 * 30 * 24 * 60 * 60);
        setcookie('super',
            (isset($_POST['super1']) ? ($_POST['super1']) : '') . (isset($_POST['super2']) ? (' ' . $_POST['super2']) : '') .
            (isset($_POST['super3']) ? (' ' . $_POST['super3']) : '') . (isset($_POST['super4']) ? (' ' . $_POST['super4']) : ''),
            time() + 365 * 30 * 24 * 60 * 60);
        setcookie('message', $_POST['message'], time() + 365 * 30 * 24 * 60 * 60);
        setcookie('check', $_POST['check'], time() + 365 * 30 * 24 * 60 * 60);
        header('Location: index.php');
        exit();
    }
    else
    {
        // Удаляем Cookies с признаками ошибок.
        setcookie('name_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('date_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('limb_error', '', 100000);
        setcookie('super_error', '', 100000);
        setcookie('check_error', '', 100000);

        // Подключаемся к базе данных
        $user = 'u17494';
        $pass = '2172210';
        $db = new PDO('mysql:host=localhost;dbname=u17494', $user, $pass);

        $name = $_POST['name'];
        $email = $_POST['email'];
        $date = $_POST['date'];
        $gender = $_POST['gender'];
        $limb = $_POST['limb'];
        $super = (isset($_POST['super1']) ? ($_POST['super1']) : '') . (isset($_POST['super2']) ? (' ' . $_POST['super2']) : '') .
            (isset($_POST['super3']) ? (' ' . $_POST['super3']) : '') . (isset($_POST['super4']) ? (' ' . $_POST['super4']) : '');
        $message = $_POST['message'];
        $check = $_POST['check'];

        // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
        if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login']))
        {
            // перезаписываем данные в БД новыми данными,
            // кроме логина и пароля.
            $db->query("
	        UPDATE anketaWithPass
	        SET name = '".$name."',
	            mail = '".$email."',
	            date = '".$date."',
	            gender = '".$gender."',
	            limb = '".$limb."',
	            super = '".$super."',
	            message = '".$message."',
	            checkk = '".$check."'
	        WHERE login='".$_SESSION['login']."';
	      	");
        }
        else
        {
            // Генерируем уникальный логин и пароль.
            $login = (string)substr((string)uniqid(), 0, 7);
            $pass = (string)rand(10000, 99999);
            // Сохраняем в Cookies.
            setcookie('login', $login);
            setcookie('pass', $pass);
            $pass = (string)md5($pass);

            // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
            // ...
            try
            {
                $stmt = $db->prepare("INSERT INTO anketaWithPass (name, mail, date, gender, limb, super, message, checkk, login, pass)" .
        "VALUES (:name, :mail, :date, :gender, :limb, :super, :message, :checkk, :login, :pass)");
                $stmt->bindParam(':name', $name);
                $stmt->bindParam(':mail', $email);
                $stmt->bindParam(':date', $date);
                $stmt->bindParam(':gender', $gender);
                $stmt->bindParam(':limb', $limb);
                $stmt->bindParam(':super', $super);
                $stmt->bindParam(':message', $message);
                $stmt->bindParam(':checkk', $check);
                $stmt->bindParam(':login', $login);
                $stmt->bindParam(':pass', $pass);
                $stmt->execute();
            }
            catch(PDOException $e)
            {
                print ('Error : ' . $e->getMessage());
                exit();
            }
        }

        session_destroy();
        mysqli_close($db);
    }
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');

    // Делаем перенаправление.
    header('Location: index.php');
}